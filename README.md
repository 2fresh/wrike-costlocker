
# [Wrike -> Costlocker](https://gitlab.com/costlocker/integrations/tree/master/pm)

## 1) Clone

* [crontab](/bin/crontab) expects that:
    * repo is cloned in `/srv/wrike-costlocker`
    * backups are saved to `/backup-wrike`
    * logs are logged to `/var/log/wrike-*.log`

```bash
git clone git@gitlab.com:2fresh/wrike-costlocker.git /srv/wrike-costlocker
cd /srv/wrike-costlocker
bin/init
```

## 2) Configure

* [`bin/init`](/bin/init) shows necessary unversioned [config files](https://gitlab.com/costlocker/integrations/tree/master/pm#configuration)

```bash
cp .env.example .env
nano .env
nano json/db.json
nano json/config.json
```

* docker database is [mounted to `var/json`](/bin/run#L51)

```bash
cat json/config.json | grep database
    "database": "var/json/db.json",
```

## 3) Check Wrike

* php is required, if script is runned locally

```bash
bin/analyze-wrike-mapping
    | Wrike | Wrike API | Costlocker | Is Space? |
    | ----- | --------- | ---------- | --------- |
    ...
bin/run "var/bin/analyze-wrike-mapping"
```

## 4) Sync

* check commands in [crontab](/bin/crontab)

```bash
# help
bin/run "bin/console"
bin/run "bin/console sync --help"
bin/run "bin/console sync:newProjects --help"
bin/run "bin/console sync:deletedEntries --help"

# dry-run sync (no --execute)
bin/run "ls -lAh var/json/"
bin/run "bin/console sync --query today --mode upsert"
```

## 5) Backup

* backup creates tgz archive
* no upload _(costlocker used rclone)_

```bash
# local test
bin/backup experimental-backup
ls -lAh ./experimental-backup
```
